﻿using common.resources;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;


namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Pacman = () => Behav()
.Init("0B PacMan3",
                new State(
                    new DropPortalOnDeath("Glowing Realm Portal", 100),
                      new Prioritize(
                            new Follow(1.2, 14, 1)
                            ),
                    new State("default",
                        new PlayerWithinTransition(8, "basic")
                        ),
                    new State("basic",
                        new Shoot(25, projectileIndex: 0, coolDown: 900),
                        new Shoot(25, 2, projectileIndex: 1, coolDown: 1350),
                        new Shoot(25, 2, predictive: 1, projectileIndex: 2, coolDown: 1350)
                        )
                    ),
                //LootTemplates.DefaultEggLoot(EggRarity.Legendary),

                new Threshold(0.025,
                    new ItemLoot("Greater Potion of Defense", 1.0),
                    new TierLoot(12, ItemType.Weapon, 0.1),
                    new TierLoot(6, ItemType.Ability, 0.1),
                    new TierLoot(12, ItemType.Armor, 0.1),
                    new TierLoot(6, ItemType.Ring, 0.05),
                    new TierLoot(13, ItemType.Armor, 0.05),
                    new TierLoot(12, ItemType.Weapon, 0.05),
                    new TierLoot(7, ItemType.Ring, 0.025),
                    new ItemLoot("Dirk of Broken Cube", 0.0019),
                    new ItemLoot("Ambrosia", 0.09),
                    new ItemLoot("Bracer of Stone", 0.05),
                    new ItemLoot("Ancient Stone Sword", 0.05)
                )
            )
        .Init("0B PacMan2",
                  new State(
                       new Prioritize(
                            new Follow(1.2, 14, 1)
                            ),
                    new State("default",
                        new PlayerWithinTransition(8, "basic")
                        ),
                    new State("basic",

                        new Shoot(25, projectileIndex: 0, coolDown: 1250)
                )
            )
            )
                .Init("0B PacMan1",
                new State(
                       new Prioritize(
                            new Follow(1.2, 14, 1)
                            ),
                    new State("default",
                        new PlayerWithinTransition(8, "basic")
                        ),
                    new State("basic",

                        new Shoot(25, projectileIndex: 0, coolDown: 1250)


            )

                    )
            )
            ;
    }
}
