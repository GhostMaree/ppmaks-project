﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using common.resources;
using wServer.realm;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ ewe = () => Behav()
            .Init("Bedlamgod",
                new State(
                    new State("1",
                         new Shoot(0, count: 16, shootAngle: 22.5, fixedAngle: 0, coolDown: 1000),
                          new Shoot(0, count: 4, shootAngle: 45.5, fixedAngle: 0, coolDown: 1000),
                          new Grenade(radius: 0.5, damage: 70, range: 0, fixedAngle: 0, coolDown: 1000),
                                new Grenade(radius: 1, damage: 170, range: 3, fixedAngle: 0, coolDown: 1000),
                                new Grenade(radius: 1, damage: 170, range: 3, fixedAngle: 90, coolDown: 1000),
                                new Grenade(radius: 1, damage: 170, range: 3, fixedAngle: 180, coolDown: 1000),
                                new Grenade(radius: 1, damage: 170, range: 3, fixedAngle: 270, coolDown: 1000),
                                new Shoot(6.3, count: 1, projectileIndex: 3, coolDown: 1700),
                                new Shoot(6.3, count: 10, projectileIndex: 0, coolDown: 5000),
                                new Shoot(6.3, count: 10, projectileIndex: 1, coolDown: 6000),
                        new SetAltTexture(1),
                        new TimedTransition(9500, "2")
                    ),
                    new State("2",
                        new TimedTransition(4500, "3"),
                         new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 0, coolDownOffset: 3000, coolDown: 15000),
                      new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 90, coolDownOffset: 3000, coolDown: 15000),
                        new Flash(color: 0x0000FF, flashPeriod: 0.1, flashRepeats: 10),

                        new Follow(1, range: 2),
                        new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 102, fixedAngle: 102, coolDown: 6000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 114, fixedAngle: 114, coolDown: 6000, coolDownOffset: 200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 126, fixedAngle: 126, coolDown: 6000, coolDownOffset: 400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 138, fixedAngle: 138, coolDown: 6000, coolDownOffset: 600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 150, fixedAngle: 150, coolDown: 6000, coolDownOffset: 800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 162, fixedAngle: 162, coolDown: 6000, coolDownOffset: 1000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 174, fixedAngle: 174, coolDown: 6000, coolDownOffset: 1200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 186, fixedAngle: 186, coolDown: 6000, coolDownOffset: 1400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 198, fixedAngle: 198, coolDown: 6000, coolDownOffset: 1600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 210, fixedAngle: 210, coolDown: 6000, coolDownOffset: 1800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 222, fixedAngle: 222, coolDown: 6000, coolDownOffset: 2000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 234, fixedAngle: 234, coolDown: 6000, coolDownOffset: 2200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 246, fixedAngle: 246, coolDown: 6000, coolDownOffset: 2400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 258, fixedAngle: 258, coolDown: 6000, coolDownOffset: 2600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 270, fixedAngle: 270, coolDown: 6000, coolDownOffset: 2800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 282, fixedAngle: 282, coolDown: 6000, coolDownOffset: 3000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 270, fixedAngle: 270, coolDown: 6000, coolDownOffset: 3200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 258, fixedAngle: 258, coolDown: 6000, coolDownOffset: 3400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 246, fixedAngle: 246, coolDown: 6000, coolDownOffset: 3600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 234, fixedAngle: 234, coolDown: 6000, coolDownOffset: 3800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 222, fixedAngle: 222, coolDown: 6000, coolDownOffset: 4000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 210, fixedAngle: 210, coolDown: 6000, coolDownOffset: 4200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 198, fixedAngle: 198, coolDown: 6000, coolDownOffset: 4400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 186, fixedAngle: 186, coolDown: 6000, coolDownOffset: 4600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 174, fixedAngle: 174, coolDown: 6000, coolDownOffset: 4800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 162, fixedAngle: 162, coolDown: 6000, coolDownOffset: 5000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 150, fixedAngle: 150, coolDown: 6000, coolDownOffset: 5200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 138, fixedAngle: 138, coolDown: 6000, coolDownOffset: 5400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 126, fixedAngle: 126, coolDown: 6000, coolDownOffset: 5600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 114, fixedAngle: 114, coolDown: 6000, coolDownOffset: 5800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 102, fixedAngle: 102, coolDown: 6000, coolDownOffset: 6000),
                        new Shoot(8.4, count: 1, fixedAngle: 0, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 90, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 180, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 270, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 208, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 190, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 130, projectileIndex: 0, coolDown: 1000),
                        new Shoot(8.4, count: 1, fixedAngle: 270, projectileIndex: 0, coolDown: 1000)

                    ),
                    new State("3",
                        new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 102, fixedAngle: 102, coolDown: 6000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 114, fixedAngle: 114, coolDown: 6000, coolDownOffset: 200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 126, fixedAngle: 126, coolDown: 6000, coolDownOffset: 400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 138, fixedAngle: 138, coolDown: 6000, coolDownOffset: 600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 150, fixedAngle: 150, coolDown: 6000, coolDownOffset: 800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 162, fixedAngle: 162, coolDown: 6000, coolDownOffset: 1000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 174, fixedAngle: 174, coolDown: 6000, coolDownOffset: 1200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 186, fixedAngle: 186, coolDown: 6000, coolDownOffset: 1400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 198, fixedAngle: 198, coolDown: 6000, coolDownOffset: 1600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 210, fixedAngle: 210, coolDown: 6000, coolDownOffset: 1800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 222, fixedAngle: 222, coolDown: 6000, coolDownOffset: 2000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 234, fixedAngle: 234, coolDown: 6000, coolDownOffset: 2200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 246, fixedAngle: 246, coolDown: 6000, coolDownOffset: 2400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 258, fixedAngle: 258, coolDown: 6000, coolDownOffset: 2600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 270, fixedAngle: 270, coolDown: 6000, coolDownOffset: 2800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 282, fixedAngle: 282, coolDown: 6000, coolDownOffset: 3000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 270, fixedAngle: 270, coolDown: 6000, coolDownOffset: 3200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 258, fixedAngle: 258, coolDown: 6000, coolDownOffset: 3400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 246, fixedAngle: 246, coolDown: 6000, coolDownOffset: 3600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 234, fixedAngle: 234, coolDown: 6000, coolDownOffset: 3800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 222, fixedAngle: 222, coolDown: 6000, coolDownOffset: 4000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 210, fixedAngle: 210, coolDown: 6000, coolDownOffset: 4200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 198, fixedAngle: 198, coolDown: 6000, coolDownOffset: 4400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 186, fixedAngle: 186, coolDown: 6000, coolDownOffset: 4600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 174, fixedAngle: 174, coolDown: 6000, coolDownOffset: 4800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 162, fixedAngle: 162, coolDown: 6000, coolDownOffset: 5000),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 150, fixedAngle: 150, coolDown: 6000, coolDownOffset: 5200),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 138, fixedAngle: 138, coolDown: 6000, coolDownOffset: 5400),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 126, fixedAngle: 126, coolDown: 6000, coolDownOffset: 5600),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 114, fixedAngle: 114, coolDown: 6000, coolDownOffset: 5800),
                            new Shoot(50, projectileIndex: 0, count: 1, shootAngle: 102, fixedAngle: 102, coolDown: 6000, coolDownOffset: 6000),
                        new TimedTransition(1500, "2")
                    ),
                    new State("4",
                         new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 0, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 90, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 180, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 270, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 45, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 135, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 225, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 315, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 0, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 90, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 180, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 270, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 45, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 135, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 225, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 315, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 90, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 180, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 30, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 60, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 120, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 150, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 240, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 180, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 330, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 30, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 60, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 120, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 150, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 240, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 180, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 30, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 1, fixedAngle: 60, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 120, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 1, fixedAngle: 150, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 1, fixedAngle: 240, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 1, fixedAngle: 0, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 1, fixedAngle: 180, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 330, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 30, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 60, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 120, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 150, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 210, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 240, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 300, coolDownOffset: 12000, coolDown: 1500),
                        new TimedTransition(5500, "5")
                    ),
                    new State("5",
                         new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 0, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 90, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 180, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 270, coolDownOffset: 3000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 45, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 135, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 225, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 315, coolDownOffset: 4000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 0, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 90, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 180, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 270, coolDownOffset: 5000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 45, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 135, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 3, fixedAngle: 225, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 7, shootAngle: 10, projectileIndex: 1, fixedAngle: 315, coolDownOffset: 6000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 180, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 30, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 60, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 120, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 150, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 240, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 8000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 180, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 30, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 60, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 120, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 150, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 240, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 9000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 0, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 180, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 11000, coolDown: 1500),
                     new Shoot(0, count: 1, fixedAngle: 220, rotateAngle: 3, coolDown: 300),
                            new Shoot(0, count: 1, fixedAngle: 230, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            new Shoot(0, count: 1, fixedAngle: 240, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 30, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 60, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 120, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 150, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 210, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 240, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 300, coolDownOffset: 11000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 0, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 90, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 180, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 1, projectileIndex: 2, fixedAngle: 270, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 330, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 30, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 60, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 120, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 150, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 210, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 2, fixedAngle: 240, coolDownOffset: 12000, coolDown: 1500),
                    new Shoot(20, 2, shootAngle: 10, projectileIndex: 1, fixedAngle: 300, coolDownOffset: 12000, coolDown: 1500),
                        new TimedTransition(4500, "6")
                    ),
                    new State("6",
                         new Shoot(0, count: 1, fixedAngle: 220, rotateAngle: 3, coolDown: 300),
                            new Shoot(0, count: 1, fixedAngle: 230, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            new Shoot(0, count: 1, fixedAngle: 240, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                            new Shoot(0, count: 16, shootAngle: 22.5, fixedAngle: 0, coolDown: 1000),
                          new Shoot(0, count: 4, shootAngle: 45.5, fixedAngle: 0, coolDown: 1000),
                            //   new Shoot(0, count: 1, fixedAngle: 250, rotateAngle: 3, coolDown: 300),
                            //   new Shoot(0, count: 1, fixedAngle: 260, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            //  new Shoot(0, count: 1, fixedAngle: 270, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                            ///   new Shoot(0, count: 1, fixedAngle: 280, rotateAngle: 3, coolDown: 300),
                            ///   new Shoot(0, count: 1, fixedAngle: 290, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            ///  new Shoot(0, count: 1, fixedAngle: 300, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                            //  new Shoot(0, count: 1, fixedAngle: 310, rotateAngle: 3, coolDown: 300),
                            // new Shoot(0, count: 1, fixedAngle: 320, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            //  new Shoot(0, count: 1, fixedAngle: 330, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                            // new Shoot(0, count: 1, fixedAngle: 340, rotateAngle: 3, coolDown: 300),
                            //    new Shoot(0, count: 1, fixedAngle: 350, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            //    new Shoot(0, count: 1, fixedAngle: 360, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                            new Shoot(0, count: 1, fixedAngle: 380, rotateAngle: 3, coolDown: 300),
                            new Shoot(0, count: 16, shootAngle: 22.5, fixedAngle: 0, coolDown: 1000),
                          new Shoot(0, count: 4, shootAngle: 45.5, fixedAngle: 0, coolDown: 1000),
                           ///   new Shoot(0, count: 1, fixedAngle: 390, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                           //    new Shoot(0, count: 1, fixedAngle: 400, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                           //      new Shoot(0, count: 1, fixedAngle: 410, rotateAngle: 3, coolDown: 300),
                           ///    new Shoot(0, count: 1, fixedAngle: 510, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                           new Shoot(0, count: 1, fixedAngle: 240, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                        new TimedTransition(6500, "7")
                    ),
                    new State("7",
                          new Shoot(20, projectileIndex: 2, count: 1, shootAngle: 15, predictive: 0, coolDown: 1000),
                                new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 40, coolDown: 2000,
                            coolDownOffset: 400),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 60, coolDown: 2000,
                            coolDownOffset: 800),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 50, coolDown: 2000,
                            coolDownOffset: 1200),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 70, coolDown: 2000,
                            coolDownOffset: 1600),
                        new TimedTransition(3500, "8")
                    ),
                    new State("8",
                         new Shoot(0, count: 1, fixedAngle: 240, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                         new Shoot(0, count: 16, shootAngle: 22.5, fixedAngle: 0, coolDown: 1000),
                          new Shoot(0, count: 4, shootAngle: 45.5, fixedAngle: 0, coolDown: 1000),
                        new TimedTransition(5500, "9")
                    ),
                    new State("9",
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 40, coolDown: 2000,
                            coolDownOffset: 400),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 60, coolDown: 2000,
                            coolDownOffset: 800),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 50, coolDown: 2000,
                            coolDownOffset: 1200),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 70, coolDown: 2000,
                            coolDownOffset: 1600),
                        new TimedTransition(2500, "10")
                    ),
                    new State("10",
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 40, coolDown: 2000,
                            coolDownOffset: 400),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 60, coolDown: 2000,
                            coolDownOffset: 800),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 50, coolDown: 2000,
                            coolDownOffset: 1200),
                        new Shoot(10, projectileIndex: 1, count: 9, shootAngle: 40, fixedAngle: 70, coolDown: 2000,
                            coolDownOffset: 1600),

                        new TimedTransition(5500, "11")
                    ),
                    new State("11",
                        new Shoot(20, 10, 15, 0, 90, coolDown: 6000, coolDownOffset: 3000),
                    new Shoot(20, 10, 15, 0, 270, coolDown: 6000, coolDownOffset: 3000),
                    new Shoot(20, 10, 15, 0, 360, coolDown: 6000, coolDownOffset: 6000),
                    new Shoot(20, 10, 15, 0, 180, coolDown: 6000, coolDownOffset: 6000),
                    new Shoot(0, count: 16, shootAngle: 22.5, fixedAngle: 0, coolDown: 1000),
                          new Shoot(0, count: 4, shootAngle: 45.5, fixedAngle: 0, coolDown: 1000),
                    new Shoot(20, 1, projectileIndex: 3, predictive: 0.1, coolDown: 5000),
                        new TimedTransition(4500, "end")
                    ),
                    new State("end",
                        new Shoot(20, 10, 15, 0, 90, coolDown: 6000, coolDownOffset: 3000),
                    new Shoot(20, 10, 15, 0, 270, coolDown: 6000, coolDownOffset: 3000),
                    new Shoot(20, 10, 15, 0, 360, coolDown: 6000, coolDownOffset: 6000),
                    new Shoot(20, 10, 15, 0, 180, coolDown: 6000, coolDownOffset: 6000),
                    new Shoot(20, 1, projectileIndex: 3, predictive: 0.1, coolDown: 5000),
                        new TimedTransition(5500, "1")
                    )
                ),
                 new Threshold(0.00005,
                    new TierLoot(10, ItemType.Weapon, 0.7),
                    new TierLoot(11, ItemType.Weapon, 0.6),
                    new TierLoot(12, ItemType.Weapon, 0.05),
                    new TierLoot(5, ItemType.Ability, 0.7),
                    new TierLoot(6, ItemType.Ability, 0.05),
                    new TierLoot(13, ItemType.Weapon, 0.07),
                    new TierLoot(14, ItemType.Weapon, 0.07),
                    new TierLoot(11, ItemType.Armor, 0.7),
                    new TierLoot(11, ItemType.Armor, 0.07),
                    new TierLoot(13, ItemType.Armor, 0.07),
                    new TierLoot(14, ItemType.Armor, 0.07),
                    new TierLoot(12, ItemType.Armor, 0.06),
                    new TierLoot(13, ItemType.Armor, 0.05),
                    new TierLoot(5, ItemType.Ring, 0.06),
                    new TierLoot(6, ItemType.Ring, 0.06),
                    new TierLoot(2, ItemType.Potion, 0.5),
                    new ItemLoot("Potion of Life", 9.18),
                    new ItemLoot("Potion of Mana", 9.18),
                    new ItemLoot("Potion of Defence", 9.18),
                    new ItemLoot("Potion of Vitality", 9.18),
                    new ItemLoot("Potion of Wisdom", 9.18),
                    new ItemLoot("Potion of Speed", 9.18),
                    new ItemLoot("Potion of Attack", 9.18),
                     new ItemLoot("Unholy Tome", 0.005),
                      new ItemLoot("Unholy Shield", 0.005),
                       new ItemLoot("Unholy Staff", 0.005),
                        new ItemLoot("Unholy Spell", 0.005),
                     new ItemLoot("Potion of Life", 9.18)
                )
            );
    }
}