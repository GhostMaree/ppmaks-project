﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Boshdy = () => Behav()
.Init("Boshy Minion",
                new State(
                    new State("1",
                        new Wander(0.3),
                        new Taunt("Protect the boshy"),
                         new Orbit(0.8, 3, 10, "Boshy", speedVariance: 0.2, radiusVariance: 0.3),
                        new Shoot(10, projectileIndex: 1, predictive: 1),
                         new TimedTransition(64234500, "2323"),
                        new State("2323"),       
                        //new Suicide(),
                        new TimedTransition(4500, "1")

                    )
                    )
            );
    }
}
