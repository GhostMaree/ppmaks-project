﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using common.resources;
using wServer.logic.behaviors;
using wServer.logic.transitions;
using wServer.logic.loot;


namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Bedwachzone = () => Behav()
            .Init("Fire Elemental",
                new State(
                    new State("1",
                        new HealSelf(100,100),
                         new Shoot(50, projectileIndex: 0, count: 2, shootAngle: 119, fixedAngle: 119, coolDown: 6000, coolDownOffset: 200),
                       new Shoot(50, projectileIndex: 0, count: 2, shootAngle: 135, fixedAngle: 119, coolDown: 6000, coolDownOffset: 200),
                       new Shoot(50, projectileIndex: 0, count: 2, shootAngle: 135, fixedAngle: 119, coolDown: 6000, coolDownOffset: 200),
                        new SetAltTexture(1),
                        new TimedTransition(2500, "2")
                    ),
                    new State("2",
                        new SetAltTexture(2),
                        new Shoot(16, count: 1, projectileIndex: 0, coolDown: 3000),
                                                new Shoot(32, count: 1, projectileIndex: 0, coolDown: 1000),
                        new TimedTransition(3500, "3")
                    ),
                    new State("3",
                        new SetAltTexture(3),
                        new HealSelf(100,100),
                        new Shoot(20, count: 1, projectileIndex: 0, predictive: 1, coolDown: 1800),
                        new Shoot(10, projectileIndex: 0, count: 10, fixedAngle: 36, rotateAngle: 18, coolDown: 500),
                        new TimedTransition(2500, "4")
                    ),
                    new State("4",
                        new SetAltTexture(1),
                         new Shoot(20, count: 1, projectileIndex: 0, predictive: 1, coolDown: 1800),
                        new Shoot(10, projectileIndex: 0, count: 10, fixedAngle: 36, rotateAngle: 18, coolDown: 500),
                        new TimedTransition(3500, "5")
                    ),
                    new State("5",
                        new SetAltTexture(2),
                         new Shoot(20, count: 1, projectileIndex: 0, predictive: 1, coolDown: 1800),
                        new Shoot(10, projectileIndex: 0, count: 10, fixedAngle: 36, rotateAngle: 18, coolDown: 500),
                        new TimedTransition(2500, "6")
                    ),
                    new State("6",
                        new SetAltTexture(3),
                         new Shoot(20, count: 1, projectileIndex: 0, predictive: 1, coolDown: 1800),
                        new Shoot(10, projectileIndex: 0, count: 10, fixedAngle: 36, rotateAngle: 18, coolDown: 500),
                        new TimedTransition(3500, "7")
                    ),
                    new State("7",
                        new SetAltTexture(1),
                        new HealSelf(100),
                         new Shoot(20, count: 1, projectileIndex: 0, predictive: 1, coolDown: 1800),
                        new Shoot(10, projectileIndex: 0, count: 10, fixedAngle: 36, rotateAngle: 18, coolDown: 500),
                        new TimedTransition(2500, "8")
                    ),
                    new State("8",
                        new SetAltTexture(2),
                        new TimedTransition(1, "9")
                    ),
                    new State("9",
                        new SetAltTexture(3),
                        new TimedTransition(1, "10")
                    ),
                    new State("10",
                        new SetAltTexture(1),
                        new TimedTransition(1, "11")
                    ),
                    new State("11",
                        new SetAltTexture(2),
                        new TimedTransition(1, "end")
                    ),
                    new State("end",
                        new SetAltTexture(3),
                        new TimedTransition(1, "1")
                    )
                ),
                new Threshold(0.0001,
                     new ItemLoot("Potion of Wisdom", 1),
                    new ItemLoot(item: "Potion of Attack", probability: 1.02),
                    new TierLoot(10, ItemType.Weapon, 0.07),
                    new TierLoot(11, ItemType.Weapon, 0.06),
                    new TierLoot(12, ItemType.Weapon, 0.005),
                    new TierLoot(5, ItemType.Ability, 0.07),
                    new TierLoot(6, ItemType.Ability, 0.005),
                    new TierLoot(11, ItemType.Armor, 0.07),
                    new TierLoot(12, ItemType.Armor, 0.06),
                    new TierLoot(13, ItemType.Armor, 0.05),
                    new TierLoot(5, ItemType.Ring, 0.06),
                    new TierLoot(2, ItemType.Potion, 0.5),
                    new ItemLoot("Destruction Spell", 0.005),
                    new TierLoot(10, ItemType.Weapon, 0.05),
                    new ItemLoot("Destruction Spell", 0.005),
                    new TierLoot(12, ItemType.Weapon, 0.05),
                    //         new ItemLoot("Quiver of Thunder", 0.2),
                    new TierLoot(13, ItemType.Weapon, 0.05),
                    new TierLoot(9, ItemType.Weapon, 0.05),
                    new TierLoot(5, ItemType.Ability, 0.05),
                    new TierLoot(6, ItemType.Ability, 0.005),
                    new TierLoot(4, ItemType.Ability, 0.005),
                    new TierLoot(6, ItemType.Ring, 0.005),
                    new ItemLoot("Lime Jungle Bay", 0.2)
                )
            );
    }
}

