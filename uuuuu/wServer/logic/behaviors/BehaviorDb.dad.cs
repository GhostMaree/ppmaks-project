﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Beacezone = () => Behav()
            .Init("Bedlamd",
                new State(
                    new State("1",
                         new Shoot(0, count: 13, fixedAngle: 220, rotateAngle: 3, coolDown: 300),
                            new Shoot(0, count: 13, fixedAngle: 230, rotateAngle: 3, coolDownOffset: 300, coolDown: 200),
                            new Shoot(0, count: 13, fixedAngle: 240, rotateAngle: 3, coolDownOffset: 600, coolDown: 100),
                        new SetAltTexture(1),
                        new TimedTransition(500, "2")
                    ),
                    new State("2",
                        new SetAltTexture(2),
                        new TimedTransition(500, "3")
                    ),
                    new State("3",
                        new SetAltTexture(3),
                        new TimedTransition(500, "4")
                    ),
                    new State("4",
                        new SetAltTexture(1),
                        new TimedTransition(500, "5")
                    ),
                    new State("5",
                        new SetAltTexture(2),
                        new TimedTransition(500, "6")
                    ),
                    new State("6",
                        new SetAltTexture(3),
                        new TimedTransition(500, "7")
                    ),
                    new State("7",
                        new SetAltTexture(1),
                        new TimedTransition(500, "8")
                    ),
                    new State("8",
                        new SetAltTexture(2),
                        new TimedTransition(500, "9")
                    ),
                    new State("9",
                        new SetAltTexture(3),
                        
                        new TimedTransition(500, "10")
                    ),
                    new State("10",
                        new SetAltTexture(1),
                        new TimedTransition(500, "11")
                    ),
                    new State("11",
                        new SetAltTexture(2),
                        new TimedTransition(500, "end")
                    ),
                    new State("end",
                        new SetAltTexture(3),
                        new TimedTransition(500, "1")
                    )
                ),
                new Threshold(0.1,
                    new ItemLoot("Blue Paradise", 0.2),
                    new ItemLoot("Pink Passion Breeze", 0.2),
                    new ItemLoot("Bahama Sunrise", 0.2),
                    new ItemLoot("Lime Jungle Bay", 0.2)
                )
            );
    }
}
