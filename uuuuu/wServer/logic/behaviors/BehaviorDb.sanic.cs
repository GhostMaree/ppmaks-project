﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using common.resources;
using wServer.realm;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;

namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Sonic = () => Behav()
.Init("Sanic",
                new State(
                    new State("1",
                        new Wander(0.3),
                        new StayBack(4,5)
                    )
                             ),
                new Threshold(0.00001,
                    new TierLoot(10, ItemType.Weapon, 0.7),
                    new TierLoot(11, ItemType.Weapon, 0.6),
                    new TierLoot(12, ItemType.Weapon, 0.05),
                    new TierLoot(5, ItemType.Ability, 0.7),
                    new TierLoot(6, ItemType.Ability, 0.05),
                    new TierLoot(13, ItemType.Weapon, 0.07),
                    new TierLoot(14, ItemType.Weapon, 0.07),
                    new TierLoot(11, ItemType.Armor, 0.7),
                    new TierLoot(11, ItemType.Armor, 0.07),
                    new TierLoot(13, ItemType.Armor, 0.07),
                    new TierLoot(14, ItemType.Armor, 0.07),
                    new TierLoot(12, ItemType.Armor, 0.06),
                    new TierLoot(13, ItemType.Armor, 0.05),
                    new TierLoot(5, ItemType.Ring, 0.06),
                    new TierLoot(6, ItemType.Ring, 0.06),
                    new TierLoot(2, ItemType.Potion, 0.5),
                    new ItemLoot("Sanic Helm", 0.009),
                    new ItemLoot("Greater Potion of life", 0.2),
                    new ItemLoot("Greater potion of defense", 0.2)
                    //new ItemLoot("Lime Jungle Bay", 0.2)
                    )
            );
    }
}
