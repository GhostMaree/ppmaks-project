﻿using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;
using common.resources;
using wServer.realm;
using wServer.logic.behaviors;
using wServer.logic.loot;
using wServer.logic.transitions;


namespace wServer.logic
{
    partial class BehaviorDb
    {
        private _ Boshy = () => Behav()
            .Init("Boshy",
                new State(
                    new State("1",
                        new Wander (0.1),
                         new Shoot(8.4, count: 40, projectileIndex: 1, coolDown: 2750),
                        new TimedTransition(3500, "2")
                    ),
                    new State("2",
                         new Shoot(8.4, count: 40, projectileIndex: 1, coolDown: 10750),
                        new TimedTransition(3500, "3")
                    ),
                    new State("3",
                         new Shoot(7.2, count: 40, projectileIndex: 2, coolDown: 7750),
                          new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 0, coolDown: 500000,maxDensity:2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 45, coolDown: 500000, maxDensity: 2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 90, coolDown: 500000,maxDensity: 2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 135, coolDown: 500000, maxDensity: 2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 180, coolDown: 500000,maxDensity: 2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 225, coolDown: 500000,maxDensity: 1),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 270, coolDown: 500000, maxDensity: 2),
                        new TossObject("Boshy Minion", range: 4, throwEffect: true, angle: 315, coolDown: 500000, maxDensity: 1),
                         new Wander(0.1),
                        new TimedTransition(3500, "4")
                    ),
                    new State("4",
                        new Taunt(true, "Is Boshy Time!!!!!!!!!!"),
                         new Shoot(7.2, count: 40, projectileIndex: 1, coolDown: 12750),
                        new TimedTransition(2500, "5")
                    ),
                    new State("5",
                         new Taunt(true, "Is Boshy Time!!!!!!!!!!"),
                          new Shoot(8.4, count: 40, projectileIndex: 2, coolDown: 12750),
                        new TimedTransition(2500, "6")
                    ),
                    new State("6",
                        new Wander(0.1),
                        new TimedTransition(2500, "7")
                    ),
                    new State("7",
                         new Shoot(8.4, count: 40, projectileIndex: 3, coolDown: 12750),
                         new Wander(0.1),
                        new TimedTransition(1500, "8")
                    ),
                    new State("8",
                        new SetAltTexture(2),
                        new Shoot(8.4, count: 40, projectileIndex: 3, coolDown: 12750),
                        new TimedTransition(9500, "9")
                    ),
                    new State("9",
                         new Shoot(8.4, count: 40, projectileIndex: 3, coolDown: 9750),
                        new TimedTransition(1500, "10")
                    ),
                    new State("10",
                         new Shoot(8.4, count: 40, projectileIndex: 3, coolDown: 4750),
                        new TimedTransition(2500, "11")
                    ),
                    new State("11",
                        new Wander(0.1),
                         new Shoot(8.4, count: 40, projectileIndex: 3, coolDown: 2750),
                        new TimedTransition(4500, "end")
                    ),
                    new State("end",
                        new TimedTransition(3500, "1")
                    )
                ),
                new Threshold(0.00001,
                    new TierLoot(10, ItemType.Weapon, 0.7),
                    new TierLoot(11, ItemType.Weapon, 0.6),
                    new TierLoot(12, ItemType.Weapon, 0.05),
                    new TierLoot(5, ItemType.Ability, 0.7),
                    new TierLoot(6, ItemType.Ability, 0.05),
                    new TierLoot(13, ItemType.Weapon, 0.07),
                    new TierLoot(14, ItemType.Weapon, 0.07),
                    new TierLoot(11, ItemType.Armor, 0.7),
                    new TierLoot(11, ItemType.Armor, 0.07),
                    new TierLoot(13, ItemType.Armor, 0.07),
                    new TierLoot(14, ItemType.Armor, 0.07),
                    new TierLoot(12, ItemType.Armor, 0.06),
                    new TierLoot(13, ItemType.Armor, 0.05),
                    new TierLoot(5, ItemType.Ring, 0.06),
                    new TierLoot(6, ItemType.Ring, 0.06),
                    new TierLoot(2, ItemType.Potion, 0.5),
                    new ItemLoot("Boshy Gun", 0.005),
                    new ItemLoot("Greater Potion of life", 0.2),
                    new ItemLoot("Greater potion of defense", 0.2)
                    //new ItemLoot("Lime Jungle Bay", 0.2)
                )
            );
    }
}

