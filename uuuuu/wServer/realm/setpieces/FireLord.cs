﻿using wServer.realm.worlds;

namespace wServer.realm.setpieces
{
    class Zarxthefirelord : ISetPiece
    {
        public int Size { get { return 5; } }

        public void RenderSetPiece(World world, IntPoint pos)
        {
            var Zarxthefirelord = Entity.Resolve(world.Manager, "Zarx the fire lord");
            Zarxthefirelord.Move(pos.X + 2.5f, pos.Y + 2.5f);
            world.EnterWorld(Zarxthefirelord);
            {
                var proto = world.Manager.Resources.Worlds["Fire"];
                SetPieces.RenderFromProto(world, pos, proto);
            }
        }
    }
}
