package com.company.assembleegameclient.laboratory {
import com.company.assembleegameclient.account.ui.Frame;
import com.company.assembleegameclient.game.GameSprite;
import com.company.ui.BaseSimpleText;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.filters.DropShadowFilter;
import flash.text.TextFieldAutoSize;


public class CollectFameConfirmation extends Frame{
        private var gs_:GameSprite;
        private var explainText_:BaseSimpleText;
        public function CollectFameConfirmation(gameSprite:GameSprite, width:int, height:int) {
            super("Are you sure?", "Agree", "Disagree");
            this.gs_ = gameSprite;
            this.w_ = width;
            this.h_ = height;
            this.leftButton_.x = 20;
            this.rightButton_.x = 200;
            leftButton_.addEventListener(MouseEvent.CLICK, this.agree_);
            rightButton_.addEventListener(MouseEvent.CLICK, this.close_);
            this.explainText_ = new BaseSimpleText(20, 0xB3B3B3, false, 0, 0);
            this.explainText_.setBold(true);
            this.explainText_.x = 20;
            this.explainText_.y = 50;
            this.explainText_.text = "This will add your base fame \ncount to your account fame, \nand reset your XP+Fame to 0. \nThis cannot be undone.";
            this.explainText_.updateMetrics();
            this.explainText_.filters = [new DropShadowFilter(0, 0, 0)];
            addChild(this.explainText_);
        }
    private function agree_(_arg1:Event): void {
        this.gs_.gsc_.playerText("/collectFame"); //need to add command that functions still
        dispatchEvent(new Event(Event.COMPLETE));
    }
    private function close_(_arg1:Event): void {
        //
    }
}
}
